(in-package #:short-lambdas)

;; (export 'syntax 'mixins)
(named-readtables:defreadtable short-lambdas::mixins
  (:macro-char #\] (get-macro-character #\) nil))
  (:macro-char #\[ 'read-left-bracket))
(named-readtables:defreadtable short-lambdas::syntax
  (:fuze :standard short-lambdas::mixins))
