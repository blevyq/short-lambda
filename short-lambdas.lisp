;;;; short-lambdas.lisp
(defpackage :short-lambdas
  (:use #:cl #:iterate #:alexandria)
  (:export
   :*arg-char* 
   :syntax
   :mixins))

(in-package #:short-lambdas)

(declaim (optimize (debug 3)))

(defvar *arg-char* #\_)


(defun get-number (symb)
  (if (symbolp symb)
   (let ((str (string symb)))
     (if (eq (char str 0) *arg-char*)
	 (read-from-string (subseq str 1) nil -1)
	 -1))
   -1))

(defun get-max-arg (lst)
  (iter (for x in (flatten lst))
	(maximizing (get-number x))))


(defun short-lambda (body)
  (let ((args (iter (for i from 0 to (get-max-arg body))
		    (collect (intern (format nil "~a~a" *arg-char* i))))))
    `(lambda (,@args )
       (declare (ignorable ,@args))
       ,@body)))

(defun read-left-bracket (stream char)
  (declare (ignore char))
  (short-lambda (read-delimited-list #\] stream nil)))
