This gives you a concise notation for defining lambda expressions, inspired by C++'s `bind` and Clojure's `#(...%n...)` read macro. 

To use it, `(ql:quickload short-lambdas)` 

Then `(named-readtable:in-readtable short-lambdas:syntax)`. 

Then say
```
[(+ _0 _1)]
```
and it compiles to 
```
(lambda (_0 _1)
  (declare (ignorable _0 _1))
  (+ _0 _1))
```
This allows for easy partial evaluation of your functions 
```
(mapcar [(+ 3 _0)] '(0 1 2 3 4))
> (3 4 5 6 7)
```
