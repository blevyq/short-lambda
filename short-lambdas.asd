;;;; short-lambdas.asd

(asdf:defsystem #:short-lambdas
  :description "Read macros for compact lambda expressions, based on C++'s bind"
  :author "Brian Levy"
  :license "Specify license here"
  :depends-on (#:alexandria #:iterate #:named-readtables)
  :components ((:file "short-lambdas")
	       (:file "readtable")))

